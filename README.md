# General tools #

Requirements
------------
 * PHP 5.4+

Installation
------------
Add the following to your composer.json:

```json
{
    "require": {
        "digital-east/tools": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/digitaleast/tools.git"
        }
    ]
}
```

Run `composer install` or `composer update` when this library was updated.

Basic usage
-----------
```php
<?php

use DigitalEast\Tools\ImageResize;

$image = new ImageResize('/path/to/image/file.jpg');
$image->crop(100, 100);
$image->save('/path/to/image/file_new.jpg');

?>
```